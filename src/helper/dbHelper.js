const MongoClient = require('mongodb').MongoClient
const dbConfig = require('../../config/api.config')
/* var connection = 'this is fail connection' */ // shared variable

const DBConnection = {
    connect: (callBack) => {
        let url = dbConfig.db.url + ':' + dbConfig.db.port + '/' + dbConfig.db.name
        MongoClient.connect(url, { useNewUrlParser: true }, (err, db) => {
            if(!err){
                //SET CONNECTION TO BE GLOBAL SO CAN BE ACESS ANYWARE
                global.db = db.db(dbConfig.db.name)
            }
            callBack(err, db)
        })
    },
    getConnection: () => {
        return connection
    }
}
module.exports = DBConnection
