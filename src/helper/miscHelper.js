const miscHelper = {
    dateNow: () => {
        let today = new Date()
        let dd = today.getDate()
        let mm = today.getMonth() + 1
        let yyyy = today.getFullYear()

        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = dd + '/' + mm + '/' + yyyy
        return today
    },
    deletePassword: (arr) => {
        for (let i = 0; i < arr.length; i++) {
            delete arr[i].password
        }
        return arr
    },
    sendRes: (res, statCode, resData) => {
        if(statCode == 200){
            res.send(statCode, {
                code: statCode,
                data: resData
            })
        }else{
            res.send(statCode, {
                code: statCode,
                message: resData
            })
        }
    },
    //This is folter for update mechanism so the data listed below won't change
    updateFilter: (data) => {
        delete data._id
        delete data.created_by
        delete data.created_date
        delete data.is_delete
        delete data.password
        delete data.code

        data = {
            $set: data
        }

        return data
    },
}

module.exports = miscHelper