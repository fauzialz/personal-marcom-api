const ObjectID = require('mongodb').ObjectID
const helper = require('../../helper/miscHelper')
const userModel = require('./userModel')
const userRepo = require('./userRepo')
const bcrypt = require('bcrypt')


const userController = {
    readAllUser : (req, res, next) => {
        userRepo.getAll((code, data) => {
            if(code == 200){
                data = helper.deletePassword(data)
            }
            helper.sendRes(res, code, data)
        })
    },
    /* Because user info on frontend not hit API,
        so this function have no use  */
    readUserById : (req, res, next) => {        
        let id = new ObjectID(req.params.id)

        userRepo.getById(id, (code, data) => {
            delete data.password
            helper.sendRes(res, code, data)
        })
    },
    createUser : (req, res, next) => {
        
        let data = req.body 
        
        data['created_date'] = helper.dateNow()
        data.password = bcrypt.hashSync(data.password, 13)
        //ACTIVATE THIS LATER
        /* data.mEmployeeId = new ObjectID(data.mEmployeeId)
        data.mRoleId = new ObjectID(data.mRoleId) */ 

        let userData = new userModel(data)

        userRepo.getByUsername(data.username, (code, data) => {
            if(code == 400){
                helper.sendRes(res, code, data)
            }else{
                userRepo.postOne(userData, (code, data) => {
                    helper.sendRes(res, code, data)
                })
            }
        })
    },
    updateUser : (req, res, next) => {
        let id = new ObjectID(req.body._id)
        let data = req.body
        userRepo.getById(id, (code, dbData) => {
            if(data.username == dbData.username){
                helper.sendRes(res, 400, 'You input the same data.' )
            }else{
                userRepo.getByUsername(data.username, (code, dbData) =>{
                    if(code == 400){
                        helper.sendRes(res, code, dbData)
                    }else{
                        data['updated_date'] = helper.dateNow()
                        data = new userModel(data)
                        data = helper.updateFilter(data)
                        
                        userRepo.putOne(id, data, (code, data) => {
                            helper.sendRes(res, code, data)
                        })
                    }
                })
            }
        })
    },
    deleteUser : (req, res, next) => {
        let id = new ObjectID(req.params.id)
        userRepo.deleteOne(id, (code, data) => {
            helper.sendRes(res, code, data)
        })
    }
}

module.exports = userController