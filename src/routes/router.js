const authController = require('./auth')
const companyController = require('./company')
const userController = require('./user')

module.exports = (server) => {
    server.get('/', authController.baseHandler)
    server.post('/api/auth/login', authController.loginHandler)

    server.get('/api/user', userController.readAllUser)
    server.get('/api/user/:id', userController.readUserById)
    server.post('/api/user', userController.createUser)
    server.put('/api/user', userController.updateUser)
    server.del('/api/user/:id', userController.deleteUser)

    server.get('/api/company', companyController.readAllCompany)
    server.post('/api/company', companyController.createCompany)
    server.put('/api/company', companyController.updateCompany)
    server.del('/api/company/:id', companyController.deleteCompany)
}