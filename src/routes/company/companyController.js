const ObjectID = require('mongodb').ObjectID
const helper = require('../../helper/miscHelper')
const idGenerator = require('../../helper/idHelper')
const companyModel = require('./companyModel')
const companyRepo = require('./companyRepo')

const companyController = {
    readAllCompany : (req, res, next) => {
        companyRepo.getAll((code, data) => {
            helper.sendRes(res, code, data)
        })
    },
    readCompanyById : (req, res, next) => {

    },
    createCompany : (req, res, next) => {
        let data = req.body

        data['created_date'] = helper.dateNow()
        let companyData = new companyModel(data)

        companyRepo.getByName(companyData.name, (code, id, data) => {
            if(code == 400){
                helper.sendRes(res, code, data)
            }else{
                companyRepo.getLastData((data) => {
                    companyData.code = idGenerator.company(data) 
                    companyRepo.postOne(companyData, (code, data) => {
                        helper.sendRes(res, code, data)
                    })
                })
            }
        })
    },
    updateCompany : (req, res, next) => {
        let id = new ObjectID(req.body._id)
        let data = req.body
        companyRepo.getById(id, (code, dbData) => {
            if(
                data.name == dbData.name &&
                data.address == dbData.address &&
                data.phone == dbData.phone &&
                data.email == dbData.email
            ){
                helper.sendRes(res, 400, 'You input the same data.')
            }else{
                companyRepo.getByName(data.name, (code, dbId, dbData) => {
                    if(code == 400 && data._id != dbId){
                        helper.sendRes(res, code, dbData)
                    }else{
                        data = new companyModel(data)
                        data['updated_date'] = helper.dateNow()
                        data = helper.updateFilter(data)
                        
                        companyRepo.putOne(id, data, (code, data) => {
                            helper.sendRes(res, code, data)
                        })
                    }
                })
            }
        })
    },
    deleteCompany : (req, res, next) => {
        let id = new ObjectID(req.params.id)
        companyRepo.deleteOne(id, (code, data) => {
            helper.sendRes(res, code, data)
        })
    }
}

module.exports = companyController