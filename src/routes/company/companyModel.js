function mCompany(companyData){
    this._id = companyData._id
    this.code = companyData.code
    this.name = companyData.name
    this.address = companyData.address
    this.phone = companyData.phone
    this.email = companyData.email
    this.is_delete = companyData.is_delete || false
    this.created_by = companyData.created_by
    this.created_date = companyData.created_date
    this.updated_by = companyData.updated_by
    this.updated_date = companyData.updated_date
}

module.exports = mCompany