const dbName = 'm_company'

const companyRepo = {
    getAll: (cb) => {
        db.collection(dbName).find({ is_delete : false}).toArray().then(docs => {
            if(docs){
                if(docs.length>0){
                    cb(200, docs)
                }else{
                    cb(507, "Data doesn't exist.")
                }
            }else{
                cb(507, 'Trouble with database.')
            }
        })
    },
    getById: (id, cb) => {
        db.collection(dbName).findOne({ _id: id }).then(doc => {
            if(doc){
                cb(200, doc)
            }else{
                cb(507, 'Trouble with database.')
            }
        })
    },
    getByName: (name, cb) => {
        db.collection(dbName).findOne({ name: name }).then(doc => {
            if(doc){
                cb(400, doc._id, 'Company name already exist.')
            }else{
                cb(200)
            }
        })
    },
    /* At this point, I don't know why I write this */
    getLastData: (cb) => {
        db.collection(dbName).find().limit(1).sort({ $natural: -1 }).toArray().then(doc => {
            if(doc){
                cb(doc)
            }
        })
    },
    postOne: (data, cb) => {
        db.collection(dbName).insertOne(data)
        .then(doc => {
            cb(201, 'A new company data has been added.')
        }).catch(err => {
            cb(507, 'Fail to insert new data.')
            console.log(err)
        })
    },
    putOne: (id, data, cb) => {
        db.collection(dbName).updateOne({_id: id}, data)
        .then(doc => {
            cb(201, 'Company data has been edited.')
        }).catch(err => {
            cb(507, 'Fail to edit the data.')
            console.log(err)
        })
    },
    deleteOne: (id, cb) => {
        db.collection(dbName).updateOne({_id: id}, {
            $set: {is_delete: true }
        }).then(doc => {
            cb(201, 'Company data has been deleted.')
        }).catch(err => {
            cb(507, 'Fail to delete the data.')
            console.log(err)
        })
    }
}

module.exports = companyRepo