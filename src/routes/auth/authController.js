const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const authConfig = require('../../../config/auth.config')

const authController = {
    baseHandler: (req, res, next) => {
        res.send(200, {
            code: 200,
            message: "Hello buddy! You succesfully access the API but the rout of / can't give you anything."
        })
    },
    loginHandler: (req, res, next) => {
        const { db } = global
        let data = req.body
        if(data){
           if(data.username && data.password){
                db.collection('m_user').findOne({
                    username: data.username,
                    is_delete: false
                }, (err, doc) => {
                    if(doc){
                        if(bcrypt.compareSync(data.password, doc.password)){
                            let token = jwt.sign(doc, authConfig.secretkey)
                            delete doc.password
                            res.send(200, {
                                code: 200,
                                message: 'Login succeed.',
                                userdata: doc,
                                token: token
                                
                            })
                        }else{
                            res.send(404, {
                                code: 404,
                                message: 'Incorrect username or password.'
                            })
                        }
                    }else{
                        res.send(400, {
                            code: 400,
                            message: 'Incorrect username or password.'
                        })
                    }
                })
            }else{
                res.send(400, {
                    code: 400,
                    message: 'Please fill the Login form.'
                })
            }
        }else{
            res.send(404, {
                code: 404,
                message: 'Request not found.'
            })
        }
    }
}

module.exports = authController