const restify = require('restify');
const config = require('./config/api.config');
const corsMiddleware = require('restify-cors-middleware');
const DBConnection = require('./src/helper/dbHelper')

//CREATE SERVER WITH RESTIFY
const server = restify.createServer({
    name: config.server.name,
    version: config.server.version
});

const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['*'],
    exposeHeaders: ['*']
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.pre(cors.preflight);
server.use(cors.actual);

//CONNECT TO MONGODB
let status
if(config.db.port != 27017){status = '[Online Mode]'}else{status = '[Offline Mode]'}
DBConnection.connect((err, db) => {
    if(err){
        console.log(err)
        process.exit()
    }else{
        console.log('\x1b[36m%s\x1b[1m','\n[DATABASE] is connected...\n'+status)
    }
})

//ROUTER TO ENDPOINT
require('./src/routes/router')(server);

//RUNING THE SERVER
server.listen(process.env.PORT || 3000, () => {
    console.log('\x1b[33m%s\x1b[1m','\n[SERVER] is running...\nName : '+config.server.name+'\nPort : '+config.port)
})


