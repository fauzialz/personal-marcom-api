'use strict'
require('dotenv').config()

module.exports = {
    port: process.env.SERVER_PORT || 3000,
    server: {
        name: process.env.SERVER_NAME || 'Marcom Public Network',
        version: process.env.SERVER_VERSION || '1.0.0'
    },
    db: {
        url: process.env.MONGODB_URL || 'mongodb://@localhost',
        port: process.env.MONGODB_PORT || 27017,
        name: process.env.MONGODB_NAME || 'db_marcom'
    }
}